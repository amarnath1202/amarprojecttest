with Ada.Text_IO;
use Ada.Text_IO;
with MyPackage;

procedure packageMain is
  A, B, Res : Integer;
  MinValue  : Integer;
begin
--comment Added
  A := 12;
  B := 12;
  Res := 0;

  MyPackage.Add (A, B, Res);
  Put_Line ("Sum = " & Res'Image);

  MinValue := MyPackage.FindMinValue (A => 12, B => 12);
  Put_Line("MinValue = " & MinValue'Image);


end packageMain;
