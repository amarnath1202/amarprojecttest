with Ada.Text_IO;
use Ada.Text_IO;
with BufferSystem;

procedure BUfferSystemMain is
  MyBuffer : BufferSystem.Buffer;
  MyChar   : Character := 'A';
  MyStr    : String(1..5) := "Hello";

begin

  BufferSystem.Load (MyBuffer, MyStr);
  BufferSystem.Get (MyBuffer, MyChar);
  Put (MyChar); -- print H

  BufferSystem.Get (MyBuffer, MyChar);
  Put (MyChar); -- print e


end BUfferSystemMain;
