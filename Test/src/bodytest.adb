with Ada.Text_IO;
use Ada.Text_IO;

package body BodyTest is
  procedure BITON (A : in out Bit) is
  begin
    A.BIT_STATUS := True;
    Put_Line(" bit is true");
  end BITON;

  procedure BITOFF (A : in out Bit) is
  begin
    A.BIT_STATUS := False;
    Put_Line(" bit is false");
  end BITOFF;

end BodyTest;
