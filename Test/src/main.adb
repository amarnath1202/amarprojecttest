with Ada.Text_IO;
use Ada.Text_IO;
with Math_Operations;

Procedure Main is
  X, Y : Integer;
  Add, Sub,Mult,Div,Remi,Expo :Integer;


begin

  Put_Line ("Enter the value for Math Operations");
  X := Integer'Value (Get_Line);
  Y := Integer'Value (Get_Line);
  Math_Operations.Add (X, Y, Add);
  Math_Operations.Sub (X, Y, Sub);
  Math_Operations.Mul (X, Y, Mult);
  Math_Operations.Div (X, Y, Div);
  Math_Operations.Remainder (X, Y, Remi);
  Math_Operations.Exponent (X, Y,Expo);


  Put_Line ("Add = " & Integer'Image (Add));
  Put_Line ("Sub = " & Integer'Image (Sub));
  Put_Line ("Mul = " & Integer'Image (Mult));
  Put_Line ("Div = " & Integer'Image (Div));
  Put_Line ("Rem = " & Integer'Image (Remi));
  Put_Line ("Exp = " & Integer'Image (Expo));

end Main;
