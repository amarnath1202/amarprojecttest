package Math_Operations is

  procedure Add (A : in Integer; B : in Integer; Res : out Integer);
  procedure Sub (A : in Integer; B : in Integer; Res : out Integer);
  procedure Mul (A : in Integer; B : in Integer; Res : out Integer);
  procedure Div (A : in Integer; B : in Integer; Res : out Integer);
  procedure Remainder (A : in Integer; B : in Integer; Res : out Integer);
  procedure Exponent  (Value : in Integer; Power : in Integer; Res : out Integer);

end Math_Operations;
