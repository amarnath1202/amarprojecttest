package body  InlinePackage is

  procedure SumOfNumber ( N : in Integer; Summation : out Integer) is
  begin
    Summation := 0;
    for I in 1 .. N loop
      Summation := Summation + I;
    end loop;
  end SumOfNumber;

end InlinePackage;
