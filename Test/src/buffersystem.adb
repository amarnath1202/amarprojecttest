package body BufferSystem is
  procedure Load ( Buf : in out Buffer; Str : in String) is
  begin
    Buf.Start := 1;
    Buf.Finish := Str'Length;
    Buf.Data (Buf.Start .. Buf.Finish) := Str;
  end Load;

  procedure Get ( Buf : in out Buffer; Char : out Character) is
  begin
    Char := Buf.Data (Buf.Start);
    Buf.Start := Buf.Start + 1;
  end Get;


end BufferSystem;
