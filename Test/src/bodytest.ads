package BodyTest is

  type Bit is private;
  procedure BITON (A : in out Bit);
  procedure BITOFF (A : in out Bit);

private
  type Bit is record
       BIT_STATUS : Boolean := FALSE;
    end record;


end BodyTest;
