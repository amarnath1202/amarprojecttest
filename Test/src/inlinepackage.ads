package InlinePackage is

  procedure SumOfNumber( N : in Integer; Summation : out Integer) with Inline;

end InlinePackage;
