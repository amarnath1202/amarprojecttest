package body MyPackage is

  procedure Add (A : in Integer; B : in Integer; Res : out Integer) is
  begin
    Res := A+B;
  end Add;

  function FindMinValue (A : in Integer; B : in Integer) return Integer is
    ReturnValue :Integer := 0;
  begin

    if A > B then
      ReturnValue := B;
    else
      ReturnValue := A;

    end if;
    return ReturnValue;
  end FindMinValue;


end MyPackage;
