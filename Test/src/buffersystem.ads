package BufferSystem is
--comment added in source file
  type Buffer is private;
  A : Integer := 10;
  procedure Load ( Buf : in out Buffer; Str : in String);
  procedure Get ( Buf : in out Buffer; Char : out Character);

private
  Max : constant Integer := 50;
  type Buffer is record
    Data   : String (1 .. Max);
    Start  : Integer := 1;
    Finish : Integer := 0;
  end record;
--comment added in source file
--comment1 added in source file
--comment2 added in source file
--comment3 added in source file
end BufferSystem;
