with Ada.Text_IO;

package body Math_Operations is

  procedure Add (A : in Integer; B : in Integer; Res : out Integer) is
  begin
    Res := A+B;
  end Add;

  procedure Sub (A : in Integer; B : in Integer; Res : out Integer) is
  begin
    Res := A-B;
  end Sub;

  procedure Mul (A : in Integer; B : in Integer; Res : out Integer) is
  begin
    Res := A * B;
  end Mul;

  procedure Div (A : in Integer; B : in Integer; Res : out Integer) is
  begin
    Res := A/B;
  end Div;

  procedure Remainder (A : in Integer; B : in Integer; Res : out Integer) is
  begin
    Res := A rem B;
  end Remainder;

  procedure Exponent  (Value : in Integer; Power : in Integer; Res : out Integer) is
  begin
    Res := Value ** Power;
  end Exponent;

end Math_Operations;
