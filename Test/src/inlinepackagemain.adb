with Ada.Text_IO;
use Ada.Text_IO;
with InlinePackage;

procedure InlinePackageMain is
  Num, Sum :Integer;
begin
  Put_Line ("Enter the number");
  Num := Integer'Value (Get_Line);

  InlinePackage.SumOfNumber (Num, Sum);
  Put_Line ("Sum = " & Integer'Image (Sum));

end InlinePackageMain;
